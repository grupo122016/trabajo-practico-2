#include <iostream>
#include "fecha.h"

using namespace std;

int main()
{
	Fecha f1;
	Fecha f2(7,4,2016);

	cout << f1.toString() << " - " << f2.toString() << endl;

	return 0;
}
